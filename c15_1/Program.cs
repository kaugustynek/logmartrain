﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c15_1
{
    class Program
    {
        static void PrzykladyStos()
        {
            var stos = new Stack();

            stos.Push(1);
            stos.Push(2);
            stos.Push(3);
            stos.Push(4);
            stos.Push(5);
            stos.Push(6);

            foreach (var item in stos)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Usuwam elementy: ");
            Console.WriteLine(stos.Pop());
            Console.WriteLine(stos.Pop());

            Console.WriteLine("Ostatni element to: ");
            Console.WriteLine(stos.Peek());
        }

        static void Main(string[] args)
        {
            // kolejka
            var kolejka = new Queue();

            kolejka.Enqueue(1);
            kolejka.Enqueue(2);
            kolejka.Enqueue(3);
            kolejka.Enqueue(4);
            kolejka.Enqueue(5);
            kolejka.Enqueue(6);

            foreach (var item in kolejka)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("Usuwam elementy: ");
            Console.WriteLine(kolejka.Dequeue());
            Console.WriteLine(kolejka.Dequeue());

            Console.WriteLine("Ostatni element to: ");
            Console.WriteLine(kolejka.Peek());
        }
    }
}

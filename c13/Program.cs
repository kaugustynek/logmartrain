﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c13
{
    class Program
    {
        static double f1()
        {
            return 3.5;
        }

        static double f2()
        {
            return 7.8;
        }

        static double g1(int x1, int x2)
        {
            return (double)x1 / x2;
        }

        static void Main(string[] args)
        {
            Func<double> f = () => 3.5;
            Console.WriteLine(f());

            f = () => 7.8;
            Console.WriteLine(f());

            Func<int, int, double> g = (x1, x2) =>
            {
                double s = x1 + x2;
                return x1 / s;
            };
            Console.WriteLine(g(1,2));

            Func<char, string, string, string> h = (a, b, c) => b + a + c;
            Console.WriteLine(h('-', "AC", "DC"));

            Action message = () => Console.WriteLine("Hello world");
            message();

            Action<string, string> s1 = (p1, p2) =>
            {
                Console.WriteLine("==========================");
                Console.WriteLine(p1 + p2);
                Console.WriteLine("==========================");
            };

            s1("Janek ", "Kowalski");
        }
    }
}

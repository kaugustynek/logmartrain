﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using _10_4;

namespace c11_1
{
    class Trojkat
    {
        public Point PointA { get; set; }
        public Point PointB { get; set; }
        public Point PointC { get; set; }

        public double LiczObwod()
        {
            return Point.Length(PointA, PointB)
                + Point.Length(PointB, PointC)
                + Point.Length(PointA, PointC);
        }

        public double LiczPole()
        {
            double p = LiczObwod() / 2;

            return Math.Sqrt(
                p*
                (p - Point.Length(PointA, PointB)) *
                (p - Point.Length(PointB, PointC)) *
                (p - Point.Length(PointA, PointC))
            );
        }

        public Trojkat(double xA, double yA, double xB, double yB, double xC, double yC)
        {
            PointA = new Point(xA, yA);
            PointB = new Point(xB, yB);
            PointC = new Point(xC, yC);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var trojkat = new Trojkat(0, 0, 3, 0, 0, 4);

            Console.WriteLine($"Obwod: {trojkat.LiczObwod()}");
            Console.WriteLine($"Pole: {trojkat.LiczPole()}");
        }
    }
}

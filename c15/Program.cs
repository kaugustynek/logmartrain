﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c15
{
    class Program
    {
        static void PokazListe(ArrayList lista)
        {
            foreach (var item in lista)
            {
                if (item is int)
                {
                    Console.WriteLine($"{item} jest typu calkowitego");
                }
                else if (item is double)
                {
                    Console.WriteLine($"{item} jest typu rzeczywistego");
                }
                else if (item is string)
                {
                    Console.WriteLine($"{item} jest typu string");
                }
                else
                {
                    Console.WriteLine($"{item} jest innego typu");
                }
            }
        }
        static void Main(string[] args)
        {
            ArrayList myAL = new ArrayList
            {
                "c#",
                "c++",
                "php",
                1,
                2.0
            };

            // dodanie elementów do listy
            //myAL.Add("c#");
            //myAL.Add("c++");
            //myAL.Add("php");
            //myAL.Add(1);
            //myAL.Add(2.0);

            PokazListe(myAL);

            // utworzenie listy do odczytu
            ArrayList myReadOnlyAL = ArrayList.ReadOnly(myAL);

            // sprawdzenie czy lista jest do odczytu
            Console.WriteLine($"myAL is {(myAL.IsReadOnly ? "read-only" : "writable")}.");
            Console.WriteLine($"myReadOnlyAL is {(myReadOnlyAL.IsReadOnly ? "read-only" : "writable")}.");

            myAL.Add("java");

            Console.WriteLine("MyAL: ");
            PokazListe(myAL);

            Console.WriteLine("ReadOnlyMyAL: ");
            PokazListe(myReadOnlyAL);

            //myAL[1] = "C+++";
            //Console.WriteLine(myAL[1]);

            myAL.Insert(2, "ruby");

            Console.WriteLine("\nDodalem ruby:");
            PokazListe(myAL);

            myAL.AddRange(new string[] { "fortran", "basic" });
            Console.WriteLine();
            PokazListe(myAL);

            myAL.InsertRange(0, new ArrayList { "Lisp", "Cobol" });
            Console.WriteLine();
            PokazListe(myAL);

            if (myAL.Contains("Lisp"))
            {
                Console.WriteLine("Tak lista ta ma zapisanego Lispa.");
            }
        }
    }
}

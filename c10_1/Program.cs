﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c10_1
{
    class Singleton
    {
        private static Singleton _instance;
        private Singleton()
        {
            _instance = null;
        }

        public static Singleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Singleton();
                }

                return _instance;
            }
        }

        public void Display()
        {
            Console.WriteLine("Pokazuje singletona");
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Singleton s = Singleton.Instance;
            //Singleton s1 = new Singleton();

            s.Display();    
        }
    }
}

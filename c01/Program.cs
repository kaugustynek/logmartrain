﻿using System;

namespace c01
{
    class Program
    {
        static void OutputOperations()
        {
            Console.WriteLine("Hello Logmar");
            Console.WriteLine("Hello Logmar");
            Console.WriteLine("Hello Logmar");
            Console.WriteLine("Hello Logmar");
            Console.WriteLine("Hello Logmar");
            Console.WriteLine("Hello Logmar\n");
            Console.WriteLine(@"Hello\tLogmar");
            Console.WriteLine("\"Logmar\" \\\\ \"Szkolenie\"");
            Console.WriteLine(@"C:\dydaktyka\programowanie iii\wyklad\W02");

            // 2 + 3 = 5
            Console.WriteLine("2 + 3 = {0}", 2 + 3);
            Console.WriteLine("{0} + {1} = {2}", 2, 3, 2 + 3);

            // łancuch tekstowy interpolowany
            Console.WriteLine($"{2} + {3} = {2 + 3}");
            Console.Write($"{2} + {3} = {2 + 3}");

            Console.WriteLine("The sum of {0} and {1} is {2}", 100, 130, 100 + 130);
            Console.WriteLine("\"Left justified in a field of width 10:{ 0, -10}\"", 99);
            Console.WriteLine("\"Right justified in a field of width 10:{ 0,10}\"", 99);

            //formatowanie liczb
            Console.WriteLine("Currency formatting - {0:C} {1:C4}", 88.8, -888.8);
            Console.WriteLine("Integer formatting - {0:D5}", 88);
            Console.WriteLine("Exponential formatting - {0:E}", 888.8);
            Console.WriteLine("Fixed-point formatting - {0:F3}", 888.8888);
            Console.WriteLine("General formatting - {0:G}", 888.8888);
            Console.WriteLine("Number formatting - {0:N}", 8888888.8);
            Console.WriteLine("Hexadecimal formatting - {0:X4}", 88);

        }

        static void DataTypesAndVariables()
        {
            // definicja zmiennej
            double d = 35.5;
            int counter = 1;
            char randomChar = 'A';
            char specialChar = '\n';
            bool test = true;

            // metody przejęte od klasy string
            Console.WriteLine(d.ToString());
            Console.WriteLine(d.Equals(35.5));
            Console.WriteLine(d.GetType());
            Console.WriteLine(d.GetHashCode());

            //  metody dostępne dla danego typu prostego
            Console.WriteLine(double.MaxValue);
            Console.WriteLine(double.MinValue);
            Console.WriteLine(double.PositiveInfinity);
            Console.WriteLine(double.NegativeInfinity);

            // konwercja string -> double
            string liczbaString = "45.5";
            double liczba = double.Parse(liczbaString);
            Console.WriteLine(liczba);
        }

        static void RectangleAreaCalculate()
        {
            //Policz pole prostokota o dlugosciach boku podanych przez uzytkownika
            double wysokosc, szerokosc;

            Console.Write("Podaj wysokosc: ");
            wysokosc = double.Parse(Console.ReadLine());

            Console.Write("Podaj szerokosc: ");
            szerokosc = double.Parse(Console.ReadLine());

            Console.WriteLine($"Pole prostokata wynosi {wysokosc * szerokosc}");
        }

        static void Operators()
        {
            Console.WriteLine((double)1 / 2); //    result 0

            Console.WriteLine(10 % 3);        //    result 1

            int i = 10;
            Console.WriteLine(i++); // 10 i = 11
            Console.WriteLine(++i); // 12
            Console.WriteLine(i--); // 12  i = 11
            Console.WriteLine(--i); // 10    
        }

        static void ParkomatSimulator()
        {
            // Napisać program symulujacy dzialanie parkomatu
            // Dane: stawka godzinowa, ilosc godzin postoju, wplacona kwota
            // Wynik: reszta (5, 2, 1zł)

            const int stawkaGodzinowa = 2;
            int iloscGodzinPostoju;
            int wplaconaKwota;

            Console.Write("Podaj ile godzin stales: ");
            iloscGodzinPostoju = int.Parse(Console.ReadLine());
            Console.Write("Podaj ile wplaciles: ");
            wplaconaKwota = int.Parse(Console.ReadLine());

            int reszta = wplaconaKwota - iloscGodzinPostoju * stawkaGodzinowa;
            Console.WriteLine($"Reszta wynosi: {reszta}");

            int ile5 = reszta / 5;
            Console.WriteLine($"Wyplacam 5zl: {ile5}");

            int ile2 = reszta % 5 / 2;
            Console.WriteLine($"Wyplacam 2zl: {ile2}");

            int ile1 = reszta % 5 % 2;
            Console.WriteLine($"Wyplacam 1zl: {ile1}");
        }

        static void Main(string[] args)
        {
            /* to jest komentarz
             * wielowierszowy
             */

            //OutputOperations();
            //DataTypesAndVariables();
            //RectangleAreaCalculate();
            //Operators();
            ParkomatSimulator();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c03
{
    // dekonstrukcja
    public class Person
    {
        public string FirstName { get; }
        public string LastName { get; }
        public int Age { get; set; }

        public Person(string first, string last)
        {
            FirstName = first;
            LastName = last;
            Age = 13;
        }

        public void Deconstruct(out string firstName, out string lastName)
        {
            firstName = FirstName;
            lastName = LastName;
        }

        public void Deconstruct(out string firstName, out string lastName, out int age)
        {
            firstName = FirstName;
            lastName = LastName;
            age = 14;
        }

    }

    class Program
    {
        static void zadanie1()
        {
            double a = 5, b = 6, c = 7;

            if (a + b > c && a + c > b && b + c > a)
            {
                if (a == b && b == c)
                {
                    Console.WriteLine("Trojkat rownoboczny");
                }
                else
                if (a == b || b == c || c == a)
                {
                    Console.WriteLine("Trojkat ramienny");
                }
                else
                if (a * a + b * b == c * c
                    || a * a + c * c == b * b
                    || b * b + c * c == a * a)
                {
                    Console.WriteLine("Trojkat prostokatny");
                }
                else
                {
                    Console.WriteLine("Roznoboczny");
                }
            }
            else
            {
                Console.WriteLine("nie mozna zbudowac trojkata");
            }
        }

        static void zadanie2()
        {
            double a = 5, b = 4, c = 3, d = 1;

            Console.Write("Przed sortowaniem: ");
            Console.WriteLine($"{a}, {b}, {c}, {d}");

            // iteracja 1
            if (a > b)
            {
                double bufor = a;
                a = b;
                b = bufor;
            }

            if (b > c)
            {
                double bufor = b;
                b = c;
                c = bufor;
            }

            if (c > d)
            {
                double bufor = c;
                c = d;
                d = bufor;
            }

            // iteracja 2
            if (a > b)
            {
                double bufor = a;
                a = b;
                b = bufor;
            }

            if (b > c)
            {
                double bufor = b;
                b = c;
                c = bufor;
            }

            // iteracja 3
            if (a > b)
            {
                double bufor = a;
                a = b;
                b = bufor;
            }

            Console.Write("Po sortowaniu: ");
            Console.WriteLine($"{a}, {b}, {c}, {d}");
        }

        static void Konwersje()
        {
            int year;
            double engineCapacity;

            Console.Write("Podaj rok produkcji auta: ");
            year = Convert.ToInt32(Console.ReadLine());         // jeśli podany string nie będzie liczbą
                                                                // zostanie rzucony wyjątek

            Console.Write("Podaj pojemność silnika: ");
            engineCapacity = Convert.ToDouble(Console.ReadLine());

            string liczba = "7,5";  // jeśli w ustawieniach regionalnych jest '.' jako      
                                    // separator dziesiętny
                                    // to podczas konwersji będzie rzucony wyjątek
                                    //Console.WriteLine($"Liczba: {Convert.ToDouble(liczba)}"); // Wyjątek: System.FormatException

            // prawidlowe rozwiazanie
            Console.WriteLine($"Liczba: {Convert.ToDouble(liczba, CultureInfo.GetCultureInfo("pl-PL"))}");
        }

        static void PatternMatching()
        {
            // Napisać program, który będzie w zależności od typu danych dokona sprawdzenia wartości
            // zmiennej obj typu object.
            // double [40,50)
            // double [50, +inf)
            // int (-inf,10)U(20,+Inf)
            // int [10, 20]

            object obj = 40.0;

            switch (obj)
            {
                case double d when d >= 40 && d < 50:
                    Console.WriteLine("Liczba rzeczywista z przedziału [40,50)");
                    break;
                case double d when d >= 50:
                    Console.WriteLine("Liczba rzeczywista z większa od 50");
                    break;
                case int calkowita when calkowita < 10 || calkowita > 20:
                    Console.WriteLine("Liczba calkowita z przedzialu (-inf,10)U(20,+Inf)");
                    break;
                case int i when i >= 10 && i <= 20:
                    Console.WriteLine("Liczba rzeczywista z przedziału [10,20]");
                    break;
                default:
                    Console.WriteLine("Liczba taka nie występuje we wzorcu.");
                    break;
            }
        }

        static void Suma10Cyfr()
        {
            // Obliczyc sume 10 cyfr wprowadzonych przez uzytkownika

            double suma = 0;
            double liczba;
            double min = double.MaxValue;
            double max = double.MinValue;

            for (int licznik = 1; licznik <= 10; licznik++)
            {
                Console.Write("Podaj liczbe: ");
                liczba = Convert.ToDouble(Console.ReadLine());
                suma += liczba;

                if (liczba < min)
                {
                    min = liczba;
                }

                if (liczba > max)
                {
                    max = liczba;
                }
            }

            Console.WriteLine($"Suma: {suma}");
            Console.WriteLine($"Min: {min}");
            Console.WriteLine($"Max: {max}");

        }

        static void GetStatistics(out double sum, out double product, out double min, out double max)
        {
            double number = 0;
            sum = 0;
            product = 1;
            min = double.MaxValue;
            max = double.MinValue;
            do
            {
                Console.Write("Podaj liczbe: ");                            
                while (!double.TryParse(Console.ReadLine(), out number))
                {
                    Console.Write("To nie jest liczba rzeczywista. Podaj ponownie liczbe: ");
                }

                if (number != 0)
                {
                    sum += number;
                    product *= number;

                    max = number > max ? number : max;
                    min = number < min ? number : min;
                }
            } while (number != 0);
        }

        private static (double, double, int) ComputeSumAndSumOfSquares(IEnumerable<double> sequence)
        {
            double sum = 0;
            double sumOfSquares = 0;
            int count = 0;

            foreach (var item in sequence)
            {
                count++;
                sum += item;
                sumOfSquares += item * item;
            }

            return (sum, sumOfSquares, count);
        }

        static (double, double, double, double) GetStatistics()
        {
            GetStatistics(out double sum, out double product, out double min, out double max);

            return (sum, product, min, max);
        }

        static void TuplesExample()
        {
            // dekonstrukcja
            var p = new Person("Althea", "Goodwin");
            //var (first, last) = p;
            var (first, last, age) = p;
            var (firstname, lastname) = p;
            Console.WriteLine($"First name: {first}, Last name: {last}, Age: {age}");
            Console.WriteLine($"First name: {firstname}, Last name: {lastname}");

            (double suma, double iloczyn, double minimum, double maksimum) = GetStatistics();

            Console.WriteLine($"Suma: {suma}, iloczyn: {iloczyn}, max: {maksimum}, min: {minimum}");
        }

        static void Main(string[] args)
        {
            Suma10Cyfr();
        }
    }
}

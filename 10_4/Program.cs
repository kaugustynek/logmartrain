﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _10_4
{

    public class Point
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Point()
        {
            X = 0;
            Y = 0;
        }
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
        public void Display()
        {

            Console.WriteLine($"X= {X}");
            Console.WriteLine($"Y= {Y}");

        }
        public void Move(double dx, double dy)
        {
            X = X + dx;
            Y = Y + dy;

        }

        public static double Length(Point p1, Point p2)
        {
            return Math.Sqrt(Math.Pow(p2.X - p1.X, 2) + Math.Pow(p2.Y - p1.Y, 2));
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}

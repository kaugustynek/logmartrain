﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c13_1
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var textReader = new StreamReader("hotele.csv"))
            {
                var csv = new CsvReader(textReader);
            
                csv.Configuration.Delimiter = ";";
                csv.Configuration.RegisterClassMap<HotelMap>();

                var hotele = csv
                    .GetRecords<Hotel>()
                    .ToList();

                // Wyszukać wszystkie hotele, których nazwa zaczyna się na literę 's', 'S'
                var hoteleNaS = hotele
                    .Where(x => x.NazwaWlasna.ToLower().StartsWith("s"))
                    .OrderByDescending(x => x.NazwaWlasna);
                //.Where(x => x.NazwaWlasna.ToLower()[0] == 's');

                //foreach (var hotel in hoteleNaS)
                //{
                //    Console.WriteLine(hotel.NazwaWlasna);
                //}

                var limits = new List<string>
                {
                    "(33)",
                    "(0 33)",
                    "+48 33"
                };


                var charakteryUslug = hotele
                    .Select(x => x.CharakterUslug)
                    .Distinct();

                foreach (var usluga in charakteryUslug)
                {
                    Console.WriteLine(usluga);
                }

                Console.WriteLine(hotele.Count());


                var hotel5gwiazdkowy = hotele
                    .Where(x => x.KategoriaObiektu == "*****")
                    .OrderBy(x => x.NazwaWlasna)
                    .Skip(3);

                foreach (var hotel in hotel5gwiazdkowy)
                {
                    Console.WriteLine($"Pierwszy hotel to: {hotel.NazwaWlasna}; {hotel.Adres}");
                }

                // Pogrupować hotele wg kategorii 
                // i zwrócić ile hoteli występuje w każdej kategorii
                var grupyKategorii = hotele
                    .GroupBy(x => x.KategoriaObiektu)
                    .Select(g => new { Kategoria = g.Key, Liczba = g.Count() });

                foreach (var grupa in grupyKategorii)
                {
                    Console.WriteLine($"Kategoria obiektu: {grupa.Kategoria}, " +
                        $"Liczba hoteli: {grupa.Liczba}");
                }
            }
        }
    }
}

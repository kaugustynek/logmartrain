﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c13_1
{
    public class Hotel
    {
        public int Id { get; set; }
        public string NazwaWlasna { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string CharakterUslug { get; set; }
        public string KategoriaObiektu { get; set; }
        public string RodzajObiektu { get; set; }
        public string Adres { get; set; }
    }

    public sealed class HotelMap : ClassMap<Hotel>
    {
        public HotelMap()
        {
            Map(m => m.Id).Name("Lp.");
            Map(m => m.NazwaWlasna).Name("Nazwa wlasna");
            Map(m => m.Telefon).Name("Telefon");
            Map(m => m.Email).Name("Email");
            Map(m => m.CharakterUslug).Name("Charakter uslug");
            Map(m => m.KategoriaObiektu).Name("Kategoria obiektu");
            Map(m => m.RodzajObiektu).Name("Rodzaj obiektu");
            Map(m => m.Adres).Name("Adres");
        }
    }
}

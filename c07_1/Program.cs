﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c07_1
{
    enum JobPostion { BackEndDeveloper, FrontEndDeveloper, FullStackDeveloper }

    struct Worker
    {
        public string Name;
        public string Surname;
        public byte Age;
        public JobPostion Position;

        public void Display()
        {
            Console.WriteLine($"Imie: {Name}");
            Console.WriteLine($"Nazwisko: {Surname}");
            Console.WriteLine($"Wiek: {Age}");
            Console.WriteLine($"Stanowisko: {Position}");
        }
    }

    struct Konto
    {
        private string NumerKonta;
        private decimal Saldo;
        private decimal Debet;
        private decimal Oprocentowanie;

        // konstruktor
        public Konto(decimal debet, decimal oprocentowanie)
        {
            Debet = debet;
            Oprocentowanie = oprocentowanie;
            Saldo = Debet;
            NumerKonta = Guid.NewGuid().ToString();
        }

        public void Wplata(decimal kwotaDoWplacenia)
        {
            Saldo += kwotaDoWplacenia;
        }

        public bool Wyplata(decimal ileWyplacic)
        {
            bool result = false;
            if (ileWyplacic <= Saldo)
            {
                Saldo -= ileWyplacic;
                result = true;
            }

            return result;
        }

        public string CzytajNumerKonta()
        {
            return NumerKonta;
        }

        public decimal CzytajSaldo()
        {
            return Saldo;
        }

        public decimal CzytajDebet()
        {
            return Debet;
        }

        public decimal CzytajOprocentowanie()
        {
            return Oprocentowanie;
        }

        public void LiczOdsetki() => Saldo += Saldo * Oprocentowanie/100;
    }

    class Program
    {
        static void Main(string[] args)
        {
            var mBank = new Konto(2000, 1.5m);

            Console.WriteLine($"Numer konta: {mBank.CzytajNumerKonta()}");
            Console.WriteLine($"Saldo: {mBank.CzytajSaldo()}");
            Console.WriteLine($"Debet: {mBank.CzytajDebet()}");
            Console.WriteLine($"Oprocentowanie: {mBank.CzytajOprocentowanie()}");

            Konto duplikatKontamBank = mBank;

            duplikatKontamBank.Wplata(1000m);

            Console.WriteLine("Pierwotne konto");
            Console.WriteLine($"Numer konta: {mBank.CzytajNumerKonta()}");
            Console.WriteLine($"Saldo: {mBank.CzytajSaldo()}");
            Console.WriteLine($"Debet: {mBank.CzytajDebet()}");
            Console.WriteLine($"Oprocentowanie: {mBank.CzytajOprocentowanie()}");

            //Console.WriteLine("Duplikat konta");
            //Console.WriteLine($"Numer konta: {duplikatKontamBank.CzytajNumerKonta()}");
            //Console.WriteLine($"Saldo: {duplikatKontamBank.CzytajSaldo()}");
            //Console.WriteLine($"Debet: {duplikatKontamBank.CzytajDebet()}");
            //Console.WriteLine($"Oprocentowanie: {duplikatKontamBank.CzytajOprocentowanie()}");

            Console.WriteLine("Zwiekszam saldo o odsetki");
            mBank.LiczOdsetki();

            Console.WriteLine("Pierwotne konto");
            Console.WriteLine($"Numer konta: {mBank.CzytajNumerKonta()}");
            Console.WriteLine($"Saldo: {mBank.CzytajSaldo()}");
            Console.WriteLine($"Debet: {mBank.CzytajDebet()}");
            Console.WriteLine($"Oprocentowanie: {mBank.CzytajOprocentowanie()}");
        }
    }
}

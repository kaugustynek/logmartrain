﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c16
{
    class Lista<T>
    {
        private T[] tab;

        public Lista(int size)
        {
            tab = new T[size];
        }

        public T GetItem(int i)
        {
            return tab[i];
        }

        public void SetItem(int i, T newValue)
        {
            tab[i] = newValue;
        }
    }

    class Program
    {
        //static void Zamien(ref double a, ref double b)
        //{
        //    // a = 1, b = 3
        //    // Zamien(a, b)
        //    // a = 3, b = 1
        //    double tmp = a;
        //    a = b;
        //    b = tmp;
        //}

        //static void Zamien(ref bool a, ref bool b)
        //{
        //    // a = 1, b = 3
        //    // Zamien(a, b)
        //    // a = 3, b = 1
        //    bool tmp = a;
        //    a = b;
        //    b = tmp;
        //}

        //static void Zamien(ref char a, ref char b)
        //{
        //    // a = 1, b = 3
        //    // Zamien(a, b)
        //    // a = 3, b = 1
        //    char tmp = a;
        //    a = b;
        //    b = tmp;
        //}

        static void Zamien<T>(ref T a, ref T b)
        {
            // a = 1, b = 3
            // Zamien(a, b)
            // a = 3, b = 1
            T tmp = a;
            a = b;
            b = tmp;
        }

        static void Main(string[] args)
        {
            var lista = new Lista<double>(10);

            lista.SetItem(3, 20);
            Console.WriteLine(lista.GetItem(3));
        }
    }
}

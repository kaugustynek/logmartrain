﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c02
{
    class Program
    {
        static double FunkcjaKwadratowa(double a, double b, double c, double x)
        {
            return a * x * x + b * x + c;
        }

        static void TestFunkcjiKwadratowej()
        {
            double wspA = 1;
            double wspB = 2;
            double wspC = 3;
            double zmX = 2;
            Console.WriteLine(FunkcjaKwadratowa(wspA, wspB, wspC, zmX)); // Ctrl+Space

            // 1*2*2 + 2*2 + 3 = 11
        }

        static void ZadanieDaty()
        {
            int day1, month1, year1;
            int day2, month2, year2;

            Console.WriteLine("--Data 1--");
            Console.WriteLine("Podaj dzien: ");
            day1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj miesiac: ");
            month1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj year: ");
            year1 = int.Parse(Console.ReadLine());

            Console.WriteLine("--Data 2--");
            Console.WriteLine("Podaj dzien: ");
            day2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj miesiac: ");
            month2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Podaj year: ");
            year2 = int.Parse(Console.ReadLine());

            string firstDateMessage = "Pierwsza data jest wczesniejsza";
            string secondDateMessage = "Druga data jest wczesniejsza";
            string theSameDatesMessage = "Obie daty sa identyczne";

            if (year1 < year2)
            {
                Console.WriteLine(firstDateMessage);
            }
            else
            {
                if (year1 > year2)
                {
                    Console.WriteLine(secondDateMessage);
                }
                else // year1 == year2
                {
                    if (month1 < month2)
                    {
                        Console.WriteLine(firstDateMessage);
                    }
                    else
                    {
                        if (month1 > month2)
                        {
                            Console.WriteLine(secondDateMessage);
                        }
                        else // month1 == month2
                        {
                            if (day1 < day2)
                            {
                                Console.WriteLine(firstDateMessage);
                            }
                            else
                            {
                                if (day1 > day2)
                                {
                                    Console.WriteLine(secondDateMessage);
                                }
                                else // day1 == day2
                                {
                                    Console.WriteLine(theSameDatesMessage);
                                }
                            }
                        }
                    }
                }
            }
        }

        static void ZadanieNajwiekszaLiczba()
        {
            double liczba1, liczba2, liczba3;

            Console.WriteLine("Podaj liczbe 1: ");
            liczba1 = double.Parse(Console.ReadLine());
            Console.WriteLine("Podaj liczbe 2: ");
            liczba2 = double.Parse(Console.ReadLine());
            Console.WriteLine("Podaj liczbe 3: ");
            liczba3 = double.Parse(Console.ReadLine());

            // todo: sprawdz i wyswietl, ktora jest najwieksza
            if (liczba1 >= liczba2 && liczba1 >= liczba3)
            {
                Console.WriteLine("Liczba 1 jest najwieksza");
            }

            if (liczba2 >= liczba1 && liczba2 >= liczba3)
            {
                Console.WriteLine("Liczba 2 jest najwieksza");
            }

            if (liczba3 >= liczba1 && liczba3 >= liczba2)
            {
                Console.WriteLine("Liczba 3 jest najwieksza");
            }
        }

        static void TestFunkcjiMatematycznych()
        {
            // Ctrl+k, Ctrl+d - automatyczne formatowanie kodu
            double x = 3;

            double licznik = Math.Log(Math.Abs(3 * Math.Pow(x, 4) + 5 * x));
            double mianownik = Math.Exp(Math.Sin(2 * x));

            Console.WriteLine($"Wynik = {licznik / mianownik}");
        }

        static void TestOcena()
        {
            // Napisać program wczytujacy ocenę jako liczbę z zakresu: 2-5
            // Program ma wyswietlac ocenę słownie (niedostateczna, etc)

            byte ocena;

            Console.WriteLine("Podaj ocene: ");
            ocena = byte.Parse(Console.ReadLine());

            switch (ocena)
            {
                case 2:
                    Console.WriteLine("Niedostateczna");
                    break;
                case 3:
                    Console.WriteLine("Dostateczna");
                    break;
                case 4:
                    Console.WriteLine("Dobra");
                    break;
                case 5:
                    Console.WriteLine("Bardzo dobra");
                    break;
                default:
                    Console.WriteLine("Ocena jest spoza przyjętego zakresu.");
                    break;
            }
        }

        static void TestKalkulatora()
        {
            double liczba1, liczba2;
            char operacja;

            Console.WriteLine("Podaj liczbe 1: ");
            liczba1 = double.Parse(Console.ReadLine());
            Console.WriteLine("Podaj liczbe 2: ");
            liczba2 = double.Parse(Console.ReadLine());

            Console.WriteLine("Podaj operacje (+, -, *, /, ^): ");
            operacja = char.Parse(Console.ReadLine());

            switch (operacja)
            {
                case '+':
                    Console.WriteLine($"{liczba1} + {liczba2} = {liczba1+liczba2}");
                    break;

                case '-':
                    Console.WriteLine($"{liczba1} - {liczba2} = {liczba1 - liczba2}");
                    break;

                case '*':
                    Console.WriteLine($"{liczba1} * {liczba2} = {liczba1 * liczba2}");
                    break;

                case '^':
                    Console.WriteLine($"{liczba1} ^ {liczba2} = {Math.Pow(liczba1, liczba2)}");
                    break;

                case '/':
                    if (liczba2 != 0)
                    {
                        Console.WriteLine($"{liczba1} / {liczba2} = {liczba1 / liczba2}");
                    }
                    else
                    {
                        Console.WriteLine("Dzielenie jest niemożliwe.");
                    }
                    break;
                default:
                    Console.WriteLine("Operacja ta nie jest obslugiwana.");
                    break;
            }
        }

        static void Main(string[] args)
        {
            TestKalkulatora();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c04
{
    enum Day { Sunday, Monday=2, Tuesday, Wednesday=9, Thursday, Friday, Saturday }
    enum JobPosition
    {
        TraineeDeveloper = 1,
        JuniorDeveloper,
        Developer,
        SeniorDeveloper,
        ProjectManager,
        TestEngineer
    }

    [Flags]
    enum Days
    {

        //000100
        None = 0,//0x0,
        Sunday = 1,//0x1,
        Monday = 2,//0x2,
        Tuesday = 4,//0x4,
        Wednesday = 8,//0x8,
        Thursday = 16,//0x10,
        Friday = 0x20,
        Saturday = 0x40

            // 00100
            // 00110
            // suma bitowa
            // 00110
    }

    class Program
    {
            
        static void Statystyki()
        {
            double number = 0;
            double sum = 0;
            double product = 1;
            double min = double.MaxValue;
            double max = double.MinValue;
            int licznik = 0;
            do
            {
                Console.Write("Podaj liczbe: ");
                number = double.Parse(Console.ReadLine());

                if (number != 0)
                {
                    ++licznik;

                    sum += number;
                    product *= number;

                    max = number > max ? number : max;
                    min = number < min ? number : min;
                }
            } while (number != 0);

            double srednia = sum / licznik;
        }

        static void GraZaDuzoZaMalo()
        {
            // 1. Wylosowanie ukrytej liczby
            var random = new Random();
            int sekret = random.Next(1, 101);

            // 2. Odgadniecie ukrytej liczby
            int liczba;
            do
            {
                Console.Write("Podaj liczbe: ");
                liczba = int.Parse(Console.ReadLine());


                if (liczba > sekret)
                {
                    Console.WriteLine("Za duzo!");
                    continue;
                }

                if (liczba < sekret)
                {
                    Console.WriteLine("Za malo!");
                    continue;
                }
                
                if (liczba == sekret)
                {
                    Console.WriteLine("Gratulacje! Odgadłes liczbe.");
                    break;
                }
            }
            while (true);
        }

        static void ZadanieZGeneratorem()
        {
            double A = 1;
            double B = 100;

            var rand = new Random();


            for (int i = 0; i < 100; i++)
            {
                double y = (B - A) * rand.NextDouble() + A;
                Console.WriteLine(y);
            }
        }

        static void ForEachPrzyklad()
        {
            //double[] tab = { 5, 6, 7, 8, 9 };
            var tab = new List<double> { 5, 6, 7, 8, 9 };
            //               0  1  2  3  4
            Console.WriteLine($"First: {tab[0]}");
            Console.WriteLine($"Last: {tab[4]}");

            double suma = 0;
            foreach (var item in tab)
            {
                suma += item;
            }

            Console.WriteLine($"Suma = {suma}");
        }

        static void EnumPrzyklad()
        {
            // definicja zmiennej
            Day today = Day.Saturday;

            // rzutowanie na int
            int dayNumber = (int)today;
            Console.WriteLine("{0} is day number #{1}.", today, dayNumber);
        }

        static void CzytajPracownika()
        {
            string imie, nazwisko;
            JobPosition stanowisko;
            int idStanowiska;

            Console.Write("Podaj imie: ");
            imie = Console.ReadLine();

            Console.Write("Podaj nazwisko: ");
            nazwisko = Console.ReadLine();

            do
            {
                Console.Write("Podaj stanowisko " +
                    "(TraineeDeveloper=1, JuniorDeveloper=2, " +
                    "Developer=3, SeniorDeveloper=4, ProjectManager=5, " +
                    "TestEngineer=6): ");
                idStanowiska = int.Parse(Console.ReadLine());
            } while (idStanowiska > 6 || idStanowiska < 1);

            stanowisko = (JobPosition)idStanowiska;

            Console.WriteLine($"{imie} {nazwisko} pracuje na stanowisku {stanowisko}.");
        }

        static void Main(string[] args)
        {

            

            CzytajPracownika();
        }
    }
}

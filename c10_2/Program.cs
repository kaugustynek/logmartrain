﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c10_2
{
    class Token
    {
        public int LineNumber()
        {
            return 10;
        }

        public virtual string Name()
        {
            return "Token";
        }
    }

    class CommentToken: Token
    {
        public new int LineNumber()
        {
            return 20;
        }

        public override string Name()
        {
            return "CommentToken";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Token t1 = new Token();
            Token t2 = new CommentToken();

            Console.WriteLine("Obiekt t1: ");
            Console.WriteLine(t1.LineNumber()); // 10
            Console.WriteLine(t1.Name());       // Token

            Console.WriteLine("Obiekt t2: ");
            Console.WriteLine(t2.LineNumber()); // 10
            Console.WriteLine(t2.Name());       // CommentToken
        }
    }
}

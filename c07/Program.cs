﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c07
{
    class NieprawidlowaGodzinaException: Exception
    {
        public NieprawidlowaGodzinaException(string message = "Nieprawidlowa godzina") : base(message)
        {
        }
    }

    class NoneRootsException : Exception
    {
        public NoneRootsException(string message = "Brak pierwiastków") : base(message)
        {
        }
    }

    

    static class StringRozszerzenia
    {
        public static int LiczSamogloski(this string s)
        {
            int counter = 0;
            foreach (var letter in s.ToLower())
            {
                if (letter == 'a' ||
                    letter == 'e' ||
                    letter == 'i' ||
                    letter == 'o' ||
                    letter == 'u' ||
                    letter == 'y' )
                {
                    ++counter;
                }
            }

            return counter;
        }
    }

    class Program
    {
        static int Fibonacci(int n)
        {
            if (n == 1 || n == 0)
                return n;
            else
                return Fibonacci(n - 1) + Fibonacci(n - 2);
        }

        static void TestFibonacci()
        {
            for (int i = 0; i <= 10; i++)
            {
                Console.WriteLine($"{i}: {Fibonacci(i)}");
            }
        }

        static void TestSamoglosek()
        {
            string slowo = Console.ReadLine();
            Console.WriteLine(slowo.LiczSamogloski());
        }

        static void PierwiastkiRownaniaKwadratowego()
        {
            double a, b, c;

            do
            {
                Console.Write("Podaj a: ");
            } while (!double.TryParse(Console.ReadLine(), out a));

            do
            {
                Console.Write("Podaj b: ");

            } while (!double.TryParse(Console.ReadLine(), out b));

            do
            {
                Console.Write("Podaj c: ");
            } while (!double.TryParse(Console.ReadLine(), out c));

            double delta = b * b - 4 * a * c;

            if (delta >= 0)
            {
                double x1 = (-b + Math.Sqrt(delta)) / (2 * a);
                double x2 = (-b - Math.Sqrt(delta)) / (2 * a);

                Console.WriteLine($"x1 = {x1}");
                Console.WriteLine($"x2 = {x2}");
            }
            else
            {
                throw new NoneRootsException();
            }
        }

        static void WyjatkiTest()
        {
            // Obsluga wyjatkow
            try
            {
                Console.WriteLine("Lącze sie z baza danych ...");
                int liczba;
                Console.Write("Podaj liczbe: ");
                liczba = int.Parse(Console.ReadLine());

                if (!(liczba >= 0 && liczba <= 23))
                {
                    throw new NieprawidlowaGodzinaException("Liczba godzin jest spoza zakresu.");
                }

                //int.TryParse(Console.ReadLine(), out liczba);
                Console.WriteLine($"Wczytales liczbe {liczba}");
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (OverflowException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.WriteLine("Zamykam baze danych");
            }

            Console.WriteLine("ByeBye");
        }

        static void Main(string[] args)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c14
{
    class Program
    {
        static void RestrictionTest()
        {
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            var liczbyParzyste = numbers
                .Where(x => x % 2 == 0);

            foreach (var liczba in liczbyParzyste)
            {
                Console.WriteLine(liczba);
            }
        }

        static void GroupingTest()
        {
            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            var grupy = numbers
                .GroupBy(x => x % 5)
                .Select(g => new { Reszta = g.Key, Liczby = g });


            foreach (var grupa in grupy)
            {
                Console.Write($"Reszta {grupa.Reszta}: ");

                foreach (var liczba in grupa.Liczby)
                {
                    Console.Write($"{liczba}, ");
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            GroupingTest();
        }
    }
}

﻿using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c14
{
    public class RealEstate
    {
        public string street { get; set; }
        public string city { get; set; }
        public string zip { get; set; }
        public string state { get; set; }
        public int beds { get; set; }
        public int baths { get; set; }
        public int sq__ft { get; set; }
        public string type { get; set; }
        public string sale_date { get; set; }
        public decimal price { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
    }

    public sealed class RealEstateMap : ClassMap<RealEstate>
    {
        public RealEstateMap()
        {
            AutoMap();
        }
    }
}

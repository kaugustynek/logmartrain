﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c12
{
    class Message
    {
        public string Subject { get; set; }
        public Client From { get; set; }
        public List<Client> To { get; set; }

        private string content;

        public virtual string Content
        {
            get { return content; }
            set { content = value; }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"Nadawca: {From.Name} {From.Surname}<{From.Email}>");

            sb.Append($"Odbiorcy: ");
            foreach (var client in To)
            {
                sb.Append($"{client.Name} {client.Surname}<{client.Email}>; ");
            }
            sb.AppendLine();

            sb.AppendLine($"Temat: {Subject}");
            sb.AppendLine($"Wiadomość: {Content}");

            return sb.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c12
{
    class Program
    {
        static void Main(string[] args)
        {
            //string zdanie = "Ala";
            //zdanie = zdanie + " ma kota";

            //double a = 2, b = 3;

            ////string zdanie2 = $"a = {a}, b = {b}";
            //string zdanie2 = string.Format("a = {0}, b = {1}", a, b);

            //string zdanie3 = string.Join("Ala", " ", "ma", " ", "kota");

            //string zdanie4 = string.Concat(new[] { "Ala\n", " ", "ma", " ", "kota." });

            //Console.WriteLine(zdanie4);

            //var janekKowalski = new Client();
            //janekKowalski.Name = "Janek";
            //janekKowalski.Surname = "Kowalski";
            //janekKowalski.Email = "jkowalski@gmail.com";

            //var janekKowalski = new Client
            //{
            //    Name = "Janek",
            //    Surname = "Kowalski",
            //    Email = "jkowalski@gmail.com",
            //};
            //var czesiekWydrzycki = new Client
            //{
            //    Name = "Czesiek",
            //    Surname = "Wydrzycki",
            //    Email = "cw@gmail.com",
            //};
            //var janekDudek = new Client
            //{
            //    Name = "Janek",
            //    Surname = "Dudek",
            //    Email = "jd@o2.pl",
            //};

            //var wiadomosc = new Message
            //{
            //    From = janekKowalski,
            //    To = new List<Client> { czesiekWydrzycki, janekDudek },
            //    Subject = "Spotkanie",
            //    Content = "Cześć, ...., Pozdrawiam Janek",
            //};

            //var wiadomoscHtml = new HtmlMessage
            //{
            //    From = janekKowalski,
            //    To = new List<Client> { czesiekWydrzycki, janekDudek },
            //    Subject = "Spotkanie",
            //    Content = "<div>Cześć, ...., Pozdrawiam Janek</div>",
            //};


            //Console.WriteLine(wiadomosc);
            //Console.WriteLine(wiadomoscHtml);

            var listaDostawcowPoczty = new List<IEmailProvider>();

            listaDostawcowPoczty.Add(new OutlookProvider());
            listaDostawcowPoczty.Add(new GmailProvider());

            foreach (var provider in listaDostawcowPoczty)
            {
                var messages = provider.GetEmails();
                foreach (var message in messages)
                {
                    provider.SendEmail(message);
                }
            }
        }
    }
}

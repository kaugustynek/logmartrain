﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c12
{
    class OutlookProvider : IEmailProvider
    {
        public List<Message> GetEmails()
        {
            return new List<Message>
            {
                new Message
                {
                    From = new Client
                    {
                        Name = "Janek",
                        Surname = "Kowalski",
                        Email = "jkowalski@gmail.com",
                    },
                    To = new List<Client>
                    {
                        new Client
                        {
                            Name = "Czesiek",
                            Surname = "Wydrzycki",
                            Email = "cw@gmail.com",
                        },
                        new Client
                        {
                            Name = "Janek",
                            Surname = "Dudek",
                            Email = "jd@o2.pl",
                        },
                    },
                    Subject = "Spotkanie",
                    Content = "Cześć, ...., Pozdrawiam Janek",
                },
                new HtmlMessage
                {
                    From = new Client
                    {
                        Name = "Janek",
                        Surname = "Kowalski",
                        Email = "jkowalski@gmail.com",
                    },
                    To = new List<Client>
                    {
                        new Client
                        {
                            Name = "Czesiek",
                            Surname = "Wydrzycki",
                            Email = "cw@gmail.com",
                        },
                        new Client
                        {
                            Name = "Janek",
                            Surname = "Dudek",
                            Email = "jd@o2.pl",
                        },
                    },
                    Subject = "Spotkanie",
                    Content = "<div>Cześć, ...., Pozdrawiam Janek</div>",
                },
            };
        }

        public void SendEmail(Message message)
        {
            Console.WriteLine("Wysylam wiadomosc: ");
            Console.WriteLine(message);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c12
{
    class HtmlMessage : Message
    {
        private string htmlContent;

        public override string Content
        {
            get => htmlContent;
            set => htmlContent = value;
        }
    }
}

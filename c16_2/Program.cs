﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace c16_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // slowniki
            var slownik = new Dictionary<string, string>
            {
                {"car", "samochod"},
                {"window", "okno"},
                {"table", "stol"},
            };

            // wyswietlenie klucz
            Console.WriteLine("Klucze:");
            foreach (var key in slownik.Keys)
            {
                Console.WriteLine(key);
            }

            Console.WriteLine("Wybrane elementy:");
            Console.WriteLine(slownik["car"]);
            Console.WriteLine(slownik["window"]);

            slownik.Add("phone", "telefon");

            slownik.Remove("car");
            if (!slownik.ContainsKey("car"))
            {
                slownik.Add("car", "auto");
            }

            Console.WriteLine("\n===============================");
            foreach (var item in slownik)
            {
                Console.WriteLine($"{item.Key}: {item.Value}");
            }


            var customers = XDocument.Load("Customers.xml")
                .Root.Elements("customer")
                .Select(nodeXML => new Customer
                {
                    Id = (string)nodeXML.Element("id"),
                    Name = (string)nodeXML.Element("name"),
                    Address = (string)nodeXML.Element("address"),
                    City = (string)nodeXML.Element("city"),
                    PostalCode = (string)nodeXML.Element("postalcode"),
                    Country = (string)nodeXML.Element("country"),
                    Phone = (string)nodeXML.Element("phone"),
                    Fax = (string)nodeXML.Element("fax"),
                    Orders = nodeXML.Element("orders").Elements("order")
                        .Select(orderXMLNode => new Order
                        {
                            Id = (int)orderXMLNode.Element("id"),
                            OrderDate = (DateTime)orderXMLNode.Element("orderdate"),
                            Total = (decimal)orderXMLNode.Element("total")
                        }).ToList()
                });
        }
    }
}

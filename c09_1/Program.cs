﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c09_1
{
    class Plyta
    {
        private string _wykonowca;

        public string Wykonawca
        {
            get => _wykonowca;
            set => _wykonowca = value;
        }

        private string _tytul;

        public string Tytul
        {
            get => _tytul;
            set => _tytul = value;
        }

        private string _miejsceWydania;

        public string MiejsceWydania
        {
            get => _miejsceWydania;
            set => _miejsceWydania = value;
        }

        private int? _rokWydania;

        public int? RokWydania
        {
            get => _rokWydania;
            set
            {
                if (value < 1900 || value > 2100)
                {
                    throw new ArgumentOutOfRangeException("Rok jest spoza założonego zakresu.");
                }

                _rokWydania = value;
            }
        }

        // ctor tab x2
        public Plyta()
        {
            ++liczbaPlyt;
            RokWydania = null;
            MiejsceWydania = string.Empty;
            Tytul = string.Empty;
            Wykonawca = string.Empty;
        }

        public Plyta(string wykonawca, string tytul, string miejsceWydania, int? rokWydania)
        {
            ++liczbaPlyt;
            RokWydania = rokWydania;
            MiejsceWydania = miejsceWydania;
            Tytul = tytul;
            Wykonawca = wykonawca;
        }

        static int liczbaPlyt = 0;

        public static int IleJestPlyt()
        {
            return liczbaPlyt;
        }
    } // Plyta

    class Program
    {
        static void Main(string[] args)
        {
            var plyta1 = new Plyta();

            Console.WriteLine($"Tytul: {plyta1.Tytul}");
            Console.WriteLine($"Wykonawca: {plyta1.Wykonawca}");
            Console.WriteLine($"RokWydania: {plyta1.RokWydania}");
            Console.WriteLine($"MiejsceWydania: {plyta1.MiejsceWydania}");

            Console.WriteLine($"Liczba plyt: {Plyta.IleJestPlyt()}");
            Console.WriteLine();

            var plyta2 = new Plyta(rokWydania:1965, wykonawca:"The Beatles", miejsceWydania: "Liverpool", tytul: "Help");

            Console.WriteLine($"Tytul: {plyta2.Tytul}");
            Console.WriteLine($"Wykonawca: {plyta2.Wykonawca}");
            Console.WriteLine($"RokWydania: {plyta2.RokWydania}");
            Console.WriteLine($"MiejsceWydania: {plyta2.MiejsceWydania}");

            Console.WriteLine($"Liczba plyt: {Plyta.IleJestPlyt()}");
            Console.WriteLine();

            var listaPlyt = new List<Plyta>
            {
                new Plyta("Michael Jackson", "Black and White", "New York", 1995),
                new Plyta("Bee Gees", "Stayin' Alive", "Massechuset", 1977),
                new Plyta("The Doors", "Light my fire", "Los Angeles", 1967),
            };

            foreach (var plyta in listaPlyt)
            {
                Console.WriteLine($"Tytul: {plyta.Tytul}");
                Console.WriteLine($"Wykonawca: {plyta.Wykonawca}");
                Console.WriteLine($"RokWydania: {plyta.RokWydania}");
                Console.WriteLine($"MiejsceWydania: {plyta.MiejsceWydania}");
                Console.WriteLine();
            }
        }
    }
}

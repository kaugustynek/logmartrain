﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c08_1
{
    class Konto
    {
        private decimal saldo;

        public decimal Saldo
        {
            get { return saldo; }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException("Saldo nie moze byc ujemne");
                }

                saldo = value;
            }
        }

        // właściwość automatyczna
        public string NumerKonta { get; set; }

        //private string numerKonta;

        //public string NumerKonta
        //{
        //    get => numerKonta;
        //    set => numerKonta = value;
        //}

        public static decimal StopaOprocentowania;
    }

    static class Configuration
    {
        public static string PathToImages = "./image";
        public static string ConnectionString = "Enova";
    }

    public static class TemperatureConverter
    {
        public static double CelsiusToFahrenheit(string temperatureCelsius)
        {
            // Convert argument to double for calculations.
            double celsius = Double.Parse(temperatureCelsius);

            // Convert Celsius to Fahrenheit.
            double fahrenheit = (celsius * 9 / 5) + 32;

            return fahrenheit;
        }

        public static double FahrenheitToCelsius(string temperatureFahrenheit)
        {
            // Convert argument to double for calculations.
            double fahrenheit = Double.Parse(temperatureFahrenheit);

            // Convert Fahrenheit to Celsius.
            double celsius = (fahrenheit - 32) * 5 / 9;

            return celsius;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(TemperatureConverter.CelsiusToFahrenheit("100"));       
            //return;

            //var config = new Configuration();
            //dynamic db = null;
            //db.ConnectToDatabase(Configuration.ConnectionString);


            Konto.StopaOprocentowania = 1.5m;

            // tworzymy obiekty
            var bankSlonski = new Konto();
            bankSlonski.Saldo =-1000m;

            var pekao = new Konto
            {
                Saldo = 40000m
            };

            pekao.Saldo += 10000;

            Console.WriteLine($"Saldo: {bankSlonski.Saldo}, Oprocentowanie: {Konto.StopaOprocentowania}");
            Console.WriteLine($"Saldo: {pekao.Saldo}, Oprocentowanie: {Konto.StopaOprocentowania}");

        }
    }
}

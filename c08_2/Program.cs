﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c08_2
{
    class Plyta
    {
        public string Wykonawca { get; set; }
        public string Tytul { get; set; }
        public string MiejsceWydania { get; set; }

        //1900-2100
        private int? rokWydania;

        public int? RokWydania
        {
            get { return rokWydania; }
            set
            {
                if (value > 2100 || value < 1900)
                {
                    throw new ArgumentOutOfRangeException("Niewlasciwy rok wydania");
                }
                rokWydania = value;
            }
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
        }
    }
}

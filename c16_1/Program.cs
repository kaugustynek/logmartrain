﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c16_1
{
    class Point
    {
        public double X { get; set; }
        public double Y { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var listaPunktow = new List<Point>
            {
                new Point { X = 10, Y = 20 },
                new Point { X = 20, Y = 30 },
            };

            Console.WriteLine("Lista (0):");
            foreach (var point in listaPunktow)
            {
                Console.WriteLine($"x = {point.X}, y = {point.Y}");
            }

            // dodanie elementu na koniec listy
            listaPunktow.Add(new Point { X = 5, Y = 5 });

            // modyfikacja elementu znajdujacego sie na liscie
            listaPunktow[1].X = 90;
            listaPunktow[1].Y = 60;

            // dodawanie elementu na poczatek listy
            listaPunktow.Insert(0, new Point { X = 7, Y = 7 });

            Console.WriteLine("Lista (1):");
            foreach (var point in listaPunktow)
            {
                Console.WriteLine($"x = {point.X}, y = {point.Y}");
            }

            listaPunktow.AddRange(new Point[]
            {
                new Point { X = 3, Y = 4 },
                new Point { X = 5, Y = 6 },
                new Point { X = 7, Y = 8 },
            });

            Console.WriteLine("Lista (2):");
            foreach (var point in listaPunktow)
            {
                Console.WriteLine($"x = {point.X}, y = {point.Y}");
            }

            listaPunktow.RemoveRange(listaPunktow.Count() - 3, 3);

            Console.WriteLine("Lista (3):");
            foreach (var point in listaPunktow)
            {
                Console.WriteLine($"x = {point.X}, y = {point.Y}");
            }
        }
    }
}

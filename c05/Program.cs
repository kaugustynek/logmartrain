﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c05
{

    [Flags]
    enum PaperStatus
    {
        BeforeReview = 0x1,
        UnderReview = 0x2,
        AfterReview = 0x4,
        Accepted = 0x8,
        Rejected = 0x10,
        ForCorrections = 0x20,
        Draft = 0x40,
        FinalPaper = 0x80
    }

    class Program
    {
        static void LiczStatystyki(out double suma, out double srednia, 
            out double max, out double min)
        {
            suma = 0;
            srednia = 0;
            max = double.MinValue;
            min = double.MaxValue;
            double liczba;
            var random = new Random();
            int licznik = 0;

            do
            {
                liczba = random.Next(0, 100);

                Console.WriteLine($"Liczba: {liczba}");

                if (liczba != 0)
                {
                    suma += liczba;
                    max = liczba > max ? liczba : max;
                    min = liczba < min ? liczba : min;
                    ++licznik;
                    srednia = suma / licznik;
                }
            } while (liczba != 0);

            Console.WriteLine($"Liczba iteracji: {licznik}");
        }

        static void TabliczkaMnozenia()
        {
            for (int row = 1; row <= 20; row++)
            {
                for (int col = 1; col <= 20; col++)
                {
                    Console.Write($"{row * col}\t");
                }

                Console.WriteLine();
            }
        }

        // Zdefiniować typ wyliczeniowy PaperStatus zawierający 
        // następujące flagi: BeforeReview, UnderReview, AfterReview, 
        // Accepted, Rejected, ForCorrections, Draft, FinalPaper.
        // Zdefiniować zmienną typu PaperStatus, a następnie ustawić status, 
        // który będzie mieć włączone 
        // flagi AfterReview, Accepted, ForCorrections, Draft.

        static double SkomplikowanaFunkcja(double x, double y)
        {
            if (x < -5 && y<-5)
            {
                return Math.Pow(x + y, 1.0 / (double)3);
            }
            else
            {
                return Math.Atan(x + y);
            }
        }

        static void AddOne(ref double n)
        {
            ++n;
        }

        // a = 1, b = 2
        // zamien(a,b)
        // a = 2, b = 1

        static void Zamien(out int a, out int b)
        {
            a = 10;
            b = 20;
            int tmp = a;
            a = b;
            b = tmp;
        }

        //static double Iloczyn(double a1, double a2)
        //{
        //    return a1 * a2;
        //}

        //static double Iloczyn(double a1, double a2, double a3)
        //{
        //    return Iloczyn(a1,a2)*a3;
        //}

        static double Iloczyn(params double[] a)
        {
            double iloczyn = 1;
            for (int i = 0; i < a.Length; i++)
            {
                iloczyn *= a[i];
            }

            return iloczyn;
        }

        static double Srednia(params double[] a)
        {
            double suma = 0;
            for (int i = 0; i < a.Length; i++)
            {
                suma += a[i];
            }

            return suma / a.Length;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(Iloczyn(2,3,4,5, 3, 4, 5));

            return;


            LiczStatystyki(out double suma, out double srednia, out double max, out double min);

            Console.WriteLine($"Suma: {suma}");
            Console.WriteLine($"Srednia: {srednia}");
            Console.WriteLine($"Max: {max}");
            Console.WriteLine($"Min: {min}");
        }
    }
}

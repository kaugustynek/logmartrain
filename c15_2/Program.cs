﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c15_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var openWith = new Hashtable
            {
                { "txt", "notepad.exe" },
                { "bmp", "paint.exe" },
                { "dib", "paint.exe" },
                { "rtf", "wordpad.exe" },
            };

            //openWith.Add("txt", "notepad.exe");
            //openWith.Add("bmp", "paint.exe");
            //openWith.Add("dib", "paint.exe");
            //openWith.Add("rtf", "wordpad.exe");

            foreach (DictionaryEntry item in openWith)
            {
                Console.WriteLine($"{item.Key}: {item.Value}");
            }

            if (openWith.ContainsKey("txt"))
            {
                openWith["txt"] = "sublimetext.exe";
                Console.WriteLine(openWith["txt"]);
            }

            openWith.Remove("txt");
        

            Console.WriteLine("Klucze wszystkie: ");
            foreach (var key in openWith.Keys)
            {
                Console.WriteLine(key);
            }

            Console.WriteLine("Wartości wszystkie: ");
            foreach (var value in openWith.Values)
            {
                Console.WriteLine(value);
            }
        }
    }
}

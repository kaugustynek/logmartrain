﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c15_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string content = File.ReadAllText("example.json");

            var list = JsonConvert.DeserializeObject<List<Person>>(content);

            Console.WriteLine(list.Count);
        }
    }
}

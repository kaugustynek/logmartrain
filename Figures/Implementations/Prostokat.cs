﻿using Figures.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Implementations
{
    public class Prostokat : Figura
    {
        private double _bokA;
        private double _bokB;

        public override double LiczObwod()
        {
            return 2 * _bokA + 2 * _bokB;
        }

        public override double LiczPole()
        {
            return _bokA * _bokB;
        }

        public override void Czytaj()
        {
            Console.Write("Podaj a: ");
            _bokA = double.Parse(Console.ReadLine());
            Console.Write("Podaj b: ");
            _bokB = double.Parse(Console.ReadLine());
        }

        public override void Drukuj()
        {
            Console.WriteLine($"Prostokat: {_bokA}x{_bokB}");
            Console.WriteLine($"Pole: {LiczPole()}");
            Console.WriteLine($"Obwod: {LiczObwod()}");
        }
    }
}

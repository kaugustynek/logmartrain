﻿using Figures.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Implementations
{
    public class Trojkat : Figura
    {
        private double _bokA;
        private double _bokB;
        private double _bokC;

        public override double LiczObwod()
        {
            return _bokA + _bokB + _bokC;
        }

        public override double LiczPole()
        {
            double p = LiczObwod()/2;
            return Math.Sqrt(p *
                (p - _bokA) *
                (p - _bokB) *
                (p - _bokC));
        }

        public override void Czytaj()
        {
            Console.Write("Podaj a: ");
            _bokA = double.Parse(Console.ReadLine());
            Console.Write("Podaj b: ");
            _bokB = double.Parse(Console.ReadLine());
            Console.Write("Podaj c: ");
            _bokC = double.Parse(Console.ReadLine());
        }

        public override void Drukuj()
        {
            Console.WriteLine($"Trojkat: {_bokA}x{_bokB}x{_bokC}");
            Console.WriteLine($"Pole: {LiczPole()}");
            Console.WriteLine($"Obwod: {LiczObwod()}");
        }
    }
}

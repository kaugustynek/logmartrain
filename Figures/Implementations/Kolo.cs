﻿using Figures.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Implementations
{
    public class Kolo : Figura
    {
        private double _promien;

        public override double LiczObwod()
        {
            return 2 *Math.PI* _promien;
        }

        public override double LiczPole()
        {
            return Math.PI*_promien * _promien;
        }

        public override void Czytaj()
        {
            Console.Write("Podaj promien: ");
            _promien = double.Parse(Console.ReadLine());
        }

        public override void Drukuj()
        {
            Console.WriteLine($"Kolo o promieniu {_promien}");
            Console.WriteLine($"Pole: {LiczPole()}");
            Console.WriteLine($"Obwod: {LiczObwod()}");
        }
    }
}

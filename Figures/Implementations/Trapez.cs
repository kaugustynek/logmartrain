﻿using Figures.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Implementations
{
    public class Trapez : Figura
    {
        private double _bokA;
        private double _bokB;
        private double _bokC;
        private double _bokD;

        public override double LiczObwod()
        {
            return _bokA + _bokB + _bokC + _bokD;
        }

        public override double LiczPole()
        {
            double a_b = _bokA - _bokB;
            return 1.0/4.0* (_bokA + _bokB) / a_b 
                * Math.Sqrt( a_b + _bokC + _bokD)
                * Math.Sqrt( a_b + _bokC - _bokD)
                * Math.Sqrt( a_b - _bokC + _bokD)
                * Math.Sqrt(-a_b + _bokC + _bokD);
        }

        public override void Czytaj()
        {
            Console.Write("Podaj a: ");
            _bokA = double.Parse(Console.ReadLine());
            Console.Write("Podaj b: ");
            _bokB = double.Parse(Console.ReadLine());
            Console.Write("Podaj c: ");
            _bokC = double.Parse(Console.ReadLine());
            Console.Write("Podaj d: ");
            _bokD = double.Parse(Console.ReadLine());
        }

        public override void Drukuj()
        {
            Console.WriteLine($"Trapez: {_bokA}x{_bokB}x{_bokC}x{_bokD}");
            Console.WriteLine($"Pole: {LiczPole()}");
            Console.WriteLine($"Obwod: {LiczObwod()}");
        }
    }
}

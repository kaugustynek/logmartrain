﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Figures.Abstractions
{
    public abstract class Figura : IDisplayable, IReadable
    {
        public abstract double LiczPole();
        public abstract double LiczObwod();

        public virtual void Czytaj()
        {
            Console.WriteLine("Nic nie czytam");
        }

        public virtual void Drukuj()
        {
            Console.WriteLine("Niczego nie wyswietlam");
        }
    }

}

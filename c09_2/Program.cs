﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace c09_2
{
    class Parkometr
    {
        public string Id { get; private set; }
        public static decimal StawkaGodzinowa { get; private set; }
        public DateTime CzasPoczatkowy { get; set; }
        public DateTime? CzasKoncowy { get; set; }

        public Parkometr()
        {
            Id = Guid.NewGuid().ToString();
            StawkaGodzinowa = 2;
            CzasPoczatkowy = DateTime.Now;
            CzasKoncowy = null;
        }

        public Parkometr(decimal stawkaGodzinowa) : this()
        {
            StawkaGodzinowa = stawkaGodzinowa;
        }

        public int CzasPostoju
        {
            get
            {
                int result;
                if (CzasKoncowy.HasValue)
                {
                    result = (CzasKoncowy.Value - CzasPoczatkowy).Seconds;
                }
                else
                {
                    result = (DateTime.Now - CzasPoczatkowy).Seconds;

                }

                return result;
            }
        }

        public void Start()
        {
            CzasPoczatkowy = DateTime.Now;
        }
        public void Stop()
        {
            CzasKoncowy = DateTime.Now;
        }

        public decimal KosztPostoju()
        {
            if (CzasKoncowy == null)
            {
                throw new ArgumentNullException("Czas koncowy jest null");
            }

            return CzasPostoju * StawkaGodzinowa;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var parkometr1 = new Parkometr();

            Console.WriteLine($"Id: {parkometr1.Id}");
            Console.WriteLine($"Czas początkowy: {parkometr1.CzasPoczatkowy}");
            Console.WriteLine($"Czas koncowy: {parkometr1.CzasKoncowy}");
            Console.WriteLine($"Stawka godzinowa: {Parkometr.StawkaGodzinowa}");
            Console.WriteLine();

            var parkometr2 = new Parkometr(3);

            Console.WriteLine($"Id: {parkometr2.Id}");
            Console.WriteLine($"Czas początkowy: {parkometr2.CzasPoczatkowy}");
            Console.WriteLine($"Czas koncowy: {parkometr2.CzasKoncowy}");
            Console.WriteLine($"Stawka godzinowa: {Parkometr.StawkaGodzinowa}");
            Console.WriteLine();

            parkometr1.Start();
            parkometr2.Start();
            System.Threading.Thread.Sleep(5000);
            parkometr1.Stop();
            System.Threading.Thread.Sleep(5000);
            parkometr2.Stop();

            Console.WriteLine($"Czas postoju - parkometr 1: {parkometr1.CzasPostoju} [s]");
            Console.WriteLine($"Koszt postoju - parkometr 1: {parkometr1.KosztPostoju()} [zl]");

            Console.WriteLine($"Czas postoju - parkometr 2: {parkometr2.CzasPostoju} [s]");
            Console.WriteLine($"Koszt postoju - parkometr 2: {parkometr2.KosztPostoju()} [zl]");
        }
    }
}

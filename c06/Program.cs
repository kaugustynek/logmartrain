﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c06
{
    class Point
    {
        public double x;
        public double y;
    }

    enum TemperatureUnit
    {
        Kelvin, Celsius, Fahrenheit
    }

    static class RozszerzeniaDoStringa
    {
        public static string Odwroc(this string s)
        {
            return new string(s.Reverse().ToArray());
        }
    }

    class Program
    {

        static void ZmniejszOJeden(out int k)
        {
            k = 20;
            --k;
        }

        static int Iloczyn(params int[] tab)
        {
            int il = 1;
            foreach (var item in tab)
            {
                il *= item;
            }

            return il;
        }
        static void ReadCoordinates(Point p)
        {
            Console.Write("Podaj x: ");
            p.x = double.Parse(Console.ReadLine());

            Console.Write("Podaj y: ");
            p.y = double.Parse(Console.ReadLine());
        }

        static double Suma(double a, double b = 1)
        {
            return a + b;
        }

        static double ConvertTemperature(double t, 
            TemperatureUnit fromUnit, TemperatureUnit toUnit = TemperatureUnit.Celsius)
        {
            double result = 0;

            switch (fromUnit)
            {
                case TemperatureUnit.Kelvin:
                    switch (toUnit)
                    {
                        case TemperatureUnit.Kelvin:
                            result = t;
                            break;
                        case TemperatureUnit.Celsius:
                            result = t - 273.15;
                            break;
                        case TemperatureUnit.Fahrenheit:
                            result = (t - 273.15) * 9.0 / 5 + 32;
                            break;
                        default:
                            break;
                    }
                    break;
                case TemperatureUnit.Celsius:
                    switch (toUnit)
                    {
                        case TemperatureUnit.Kelvin:
                            result = t + 273.15;
                            break;
                        case TemperatureUnit.Celsius:
                            result = t;
                            break;
                        case TemperatureUnit.Fahrenheit:
                            result = t * 9.0 / 5 + 32;
                            break;
                        default:
                            break;
                    }
                    break;
                case TemperatureUnit.Fahrenheit:
                    switch (toUnit)
                    {
                        case TemperatureUnit.Kelvin:
                            result = (t - 32) * 5.0 / 9 + 273.15;
                            break;
                        case TemperatureUnit.Celsius:
                            result = (t - 32) * 5.0 / 9;
                            break;
                        case TemperatureUnit.Fahrenheit:
                            result = t;
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            return result;
        }

        static int Silnia(int n)
        {
            if (n == 0)
            {
                return 1;
            }
            else
            {
                return Silnia(n - 1) * n;
            }
        }

        static void Main(string[] args)
        {
            // Obsluga wyjatkow

            try
            { 
                int liczba;
                Console.Write("Podaj liczbe: ");
                liczba = int.Parse(Console.ReadLine());
                Console.WriteLine($"Wczytales liczbe {liczba}");
            }
            catch (FormatException ex)
            {
                Console.WriteLine("Podales litery zamiast cyfr");
                Console.WriteLine(ex.Source);
                Console.WriteLine(ex.ToString());
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

            Console.WriteLine("ByeBye");

            //string nazwa = "name";

            //Console.WriteLine(nazwa.Odwroc());

            //for (int i = 0; i <= 10; i++)
            //{
            //    Console.WriteLine($"{i}! = {Silnia(i)}");
            //}

            //// shift+alt+arrows
            //Console.WriteLine(ConvertTemperature(fromUnit:TemperatureUnit.Fahrenheit, t: 212, toUnit: TemperatureUnit.Celsius));
            //Console.WriteLine(ConvertTemperature(212, TemperatureUnit.Fahrenheit, TemperatureUnit.Kelvin));
            //Console.WriteLine(ConvertTemperature(212, TemperatureUnit.Fahrenheit));
        }
    }
}

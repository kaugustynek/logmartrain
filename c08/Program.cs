﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c08
{
    struct Autor
    {
        public string Imie;
        public string Nazwisko;
    }

    struct Ksiazka
    {
        public Autor AutorKsiazki;
        public string Tytul;
        public DateTime DataEwidencji;
        public DateTime? DataWycofania;
        public int RokWydania;

        public Ksiazka(string imie, string nazwisko, string tytul, int rokWydania)
        {
            AutorKsiazki.Imie = imie;
            AutorKsiazki.Nazwisko = nazwisko;
            Tytul = tytul;
            RokWydania = rokWydania;

            DataEwidencji = DateTime.Now;
            DataWycofania = null;
        }

        public void PokazKsiazke()
        {
            Console.WriteLine($"Autor: {AutorKsiazki.Imie} {AutorKsiazki.Nazwisko}");
            Console.WriteLine($"Tytul: {Tytul}");
            Console.WriteLine($"Rok wydania: {RokWydania}");
            Console.WriteLine($"Data ewidencji: {DataEwidencji}");
            Console.WriteLine($"Data wycofania: {DataWycofania}");
        }

        public void WycofajKsiazke()
        {
            DataWycofania = DateTime.Now;
        }
    }

    class Program
    {
        static void Main()
        {
            var lalka = new Ksiazka("Boleslaw", "Prus", "Lalka", 1980);
            var krzyzacy = new Ksiazka("Henryk", "Sienkiewicz", "Krzyzacy", 2000);

            lalka.PokazKsiazke();
            Console.WriteLine();
            krzyzacy.PokazKsiazke();

            System.Threading.Thread.Sleep(5000);

            krzyzacy.WycofajKsiazke();
            krzyzacy.PokazKsiazke();
        }
    }
}
